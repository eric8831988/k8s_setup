#!/bin/sh

KUBECTL="/usr/bin/kubectl"

# Get only nodes which are not drained yet
NOT_READY_NODES=$($KUBECTL get nodes | grep -P 'NotReady(?!,SchedulingDisabled)' | awk '{print $1}' | xargs echo)
# Get only nodes which are still drained
READY_NODES=$($KUBECTL get nodes | grep '\sReady,SchedulingDisabled' | awk '{print $1}' | xargs echo)

#####
METRICES_CHECK_NODE=$($KUBECTL top node | grep '\s<unknown>' | awk '{print $1}' | xargs echo)
NAMESPACES=$($KUBECTL get ns | grep -v "kube\|system\|NAME" |awk '{print $1}' | xargs echo)

#####
echo "Unready nodes that are undrained: $NOT_READY_NODES"
echo "Ready nodes: $READY_NODES"
echo "Metrices check Node Alive : $METRICES_CHECK_NODE"
echo "Pod check alive ... "

for metrices_check_node in $METRICES_CHECK_NODE;do
  echo "$metrices_check_node drain~~~~~"
  
  $KUBECTL drain --ignore-daemonsets --force $metrices_check_node &
  echo "Done"
done;


for ns in $NAMESPACES;do
  CHECK_POD_ALIVE=$($KUBECTL get pod -n $ns -o jsonpath="{..podIP}")
  for ip in $CHECK_POD_ALIVE;do
    CHECK_RESULT=$(ping -c 1 -W 1 $ip | grep ttl)
    #echo $CHECK_RESULT
    if [ -z "$CHECK_RESULT" ]
    then
      POD_NAME=$($KUBECTL get pod -n $ns -o wide | grep $ip |awk '{print $1}')
      NODE_IP=$($KUBECTL get pod $POD_NAME -n $ns -o jsonpath="{..hostIP}")
      NODE_NAME=$($KUBECTL get pod -n $ns -o wide | grep $ip |awk '{print $7}')
      
      echo "kill $POD_NAME Pod because k8s didn't get HeartBeat... "
      $KUBECTL delete pod $POD_NAME -n $ns --force --grace-period 0
      echo "done"

    fi
  done;
done;



for node in $NOT_READY_NODES; do
  echo "Node $node not drained yet, draining..."
  $KUBECTL drain --ignore-daemonsets --force $node &
  echo "Done"
done;

for node in $READY_NODES; do
  echo "Node $node still drained, uncordoning..."
  $KUBECTL uncordon $node &
  echo "Done"
done;