###### tags: `Docker&k8s`
# 使用ansible安裝k8s手冊(中文版)

透過 [Infrastructure as Code](https://en.wikipedia.org/wiki/Infrastructure_as_code) 來管控與設定系統狀態，並且達成快速自動化部署多台伺服器的一套軟體工具。

- 以 YML 格式撰寫，容易上手與維護
- 使用 SSH 與遠端伺服器群溝通
- 不需安裝中間代理(ansible)在遠端伺服器群

基於以上特性，我們可以透過 Anisble 快速的部署 Kubernetes Cluster 。
在本範例中 Kubernetes Control Plane 為 Single Master Node。

# 專案結構
- `site.yaml` 作為 ansible 主要的執行程式

- `roles` 劃分不同種類的工作內容
    - `roles/k8s-common` 針對**所有伺服器**都需要執行的設定(安裝 docker、kubeadm 等等)
    - `roles/master` 針對 **master 伺服器** 需要執行的設定(安裝 kubectl 以及設定 dns 等等)
    - `roles/worker` 針對 **worker 伺服器** 需要執行的設定 (加入 K8s cluster)
    
- `hosts` 為 ansible 要讀取的 [inventory](https://chusiang.github.io/ansible-docs-translate/intro_inventory.html)，用來註冊需要被遠端控制的伺服器群



# 安裝 Ansible

在工作電腦上安裝 Ansible

worker和master都要安裝ansible

```
$ sudo apt update
$ sudo apt install software-properties-common
$ sudo apt-add-repository --yes --update ppa:ansible/ansible
$ sudo apt install ansible
```

## Other platform

[Installing Ansible on other platforms](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#)


# 設定安裝 k8s cluster 的環境配置
### 設定/etc/hosts和/etc/ansible/hosts


* 在**控制端(master主機)** 中將**被控端(master和worker)** 的IP加入/etc/hosts
* 註: master同時身為**控制端**也是**被控端**
```sh=
vim /etc/hosts
```
![](https://i.imgur.com/3PqgZ1a.png)
* 在**控制端(master)** 主機中加入**被控端(worker)** 的IP
```shell=
vim /etc/ansible/hosts
```
![](https://i.imgur.com/xfP9z8O.png)

### 被控端產生公鑰並將公鑰傳至控制方
**要注意的是，master的主機也要做以下事情，因為即使ansible的server是在master執行，但是master自己本身也是"被控端"，所以，也要做以下步驟，換句話說，任何有需要安裝的節點都需要複製公鑰，否則安裝上無法控制**
```shell=
# 產生ssh金鑰對
ssh-keygen
ssh-agent bash
# 將ssh私鑰加入id_rsa
ssh-add ~/.ssh/id_rsa
# 將ssh金鑰對複製到ansible控制方主機
ssh-copy-id -i  ~/.ssh/id_rsa <username>@<server_host>
```

## 修改infra_setup/k8s_setup/hosts 文件，用來讓 Ansible 進行 SSH 連線
infra_setup/k8s_setup/hosts是專案的路徑，應該要依照自己的ansible環境進行動態調整
```shell=
cat infra_setup/k8s_setup/hosts
```
設定masters和workers群組的機器username
```shell=
[masters]
master ansible_user=pdclab

[workers]
worker ansible_user=pdclab 

[all:vars]
# ansible連線方式使用ssh
ansible_connection=ssh
# ansible ssh private key檔案放在家目錄底下.ssh中的id_rsa
ansible_ssh_private_key_file=~/.ssh/id_rsa 
```

## 使用ansible一鍵安裝
上述都配置完成後，使用ansible指令一鍵安裝
```shell=
# site.yaml為指定要使用的yaml配置檔，--inventory-file有點像是身分識別，指定為上方配置的hosts檔案，-kK表示使用ssh password的方式連線
ansible-playbook site.yaml --inventory-file hosts –kK
```


# 手動完整移除K8s cluster
```shell=
kubeadm reset
sudo apt-get purge kubeadm kubectl kubelet kubernetes-cni kube*   
sudo apt-get autoremove  
sudo rm -rf ~/.kube
```
[How to completely uninstall kubernetes](https://stackoverflow.com/questions/44698283/how-to-completely-uninstall-kubernetes)



## 備註
* 1.24版之後需要額外安裝cri-dockerd，因為k8s移除對於docker-shim的依賴，所以還是堅持要用docker當作runtime的話，就要用cri-dockerd讓CRI可以跟docker runtime溝通


# 安裝過程中碰到的bug和需要套件之安裝方法等


## E: The list of sources could not be read.

### Problem
* [安裝 Kubernetes 流程](https://www.letscloud.io/community/how-to-install-kubernetesk8s-and-docker-on-ubuntu-2004)
    * Step 5 - Add Software Repositories: 出error
        * ``` $sudo apt-add-repository "deb http://apt.kubernetes.io/kubernetes-xenial main" ```

### Ansible - Error Log
![](../../../public/apt_sourcesCouldNotBeRead.jpg)

### sudo apt update - Error Log
```
$ sudo apt update

E: Conflicting values set for option Signed-By regarding source https://download.docker.com/linux/ubuntu/ focal: /etc/apt/keyrings/docker.gpg !=
E: The list of sources could not be read.
```
### Solution
```
sudo rm /etc/apt/keyrings/docker.gpg
sudo rm /etc/apt/sources.list.d/docker.list
```
#### 參考資料
[Solution](https://unix.stackexchange.com/questions/732030/conflicting-values-set-for-option-signed-by-regarding-source)


## 雲端簡報:
https://docs.google.com/presentation/d/1TWuhInvbl2Xe-xC3jTPc2h90pXGs-LxM7YWJAmWGVSQ/edit#slide=id.p

### [ERROR CRI]: container runtime is not running:
主要原因是 cri (container runtime interface) 出現問題
* 未啟動
    * 本專案使用 : cri-docker.sock
* 有多個啟動中的 cri
    * containerd
    * cri-docker
    * cri-o

### kubeadm init error: CRI v1 runtime API is not implemented 
* 查看 cri 是否為被成功啟動
* 檢查 kubelet、kubectl、kubeadm 版本是否一致
* 重新執行下方kube-apiserver.yaml already exists的步驟
    * kubeadm cri 需要額外添加參數
        * --cri-socket unix:///run/cri-dockerd.sock 


### kube-apiserver.yaml already exists
此問題顯示意味著，你需要執行 kubeadm reset ，讓環境執行過 kubeadm init 重置

#### [cri-docker]
```
$ sudo kubeadm reset --cri-socket unix:///run/cri-dockerd.sock
$ sudo kubeadm init --apiserver-advertise-address=0.0.0.0 --pod-network-cidr 10.244.0.0/16 --cri-socket unix:///run/cri-dockerd.sock 
```
#### [containerd]
```
$ kubeadm reset  --cri-socket  unix:///run/containerd/containerd.sock
$ kubeadm init --pod-network-cidr=10.244.0.0/16 --upload-certs --kubernetes-version=v1.26.2 --cri-socket unix:///run/containerd/containerd.sock --apiserver-advertise-address=0.0.0.0 
```

### `kubeadm init`: tcp 127.0.0.1:10248: connect: connection refused.
意味著 kubelet 尚未啟動成功，因此需要查找相關原因，並進行除錯
#### Kubelet 服務失敗  - journalctl 查看
* 服務狀態查詢
```
$ systemctl status kubelet
------
output:

● kubelet.service - kubelet: The Kubernetes Node Agent
   Loaded: loaded (/usr/lib/systemd/system/kubelet.service; enabled; vendor preset: disabled)
  Drop-In: /usr/lib/systemd/system/kubelet.service.d
           └─10-kubeadm.conf
   Active: activating (auto-restart) (Result: exit-code) since Mon 2022-05-30 13:59:51 +08; 822ms ago
     Docs: https://kubernetes.io/docs/
  Process: 9325 ExecStart=/usr/bin/kubelet $KUBELET_KUBECONFIG_ARGS $KUBELET_CONFIG_ARGS $KUBELET_KUBEADM_ARGS $KUBELET_EXTRA_ARGS (code=exited, status=1/FAILURE)
 Main PID: 9325 (code=exited, status=1/FAILURE)
```

* 服務Log 查看
```
$ journalctl -u kubelet -f
----

```
* 參數說明:
    * -u : 搭妹服務名稱
    * -f : 取得最新的 Log



##### `Err1` : kubelet CRI v1 runtime API is not implemented for endpoint \"unix:///var/run/containerd
* kubeadm cri 需要額外添加參數
    * --cri-socket unix:///run/cri-dockerd.sock 


##### `Err2` :failed to set feature gates from initial flags-based config: unrecognized feature gate: runasgroup
* 到kubelet 配置參數的檔案修正
```!
$  sudo nano /etc/default/kubelet
----
output: 
-
KUBELET_EXTRA_ARGS=--feature-gates="AllAlpha=false,RunAsGroup=true" --container-runtime=remote --cgroup-driver=systemd --container-runtime-endpoint='unix:///run/cri-dockerd.sock' --runtime-request-timeout=5m
-----
將多餘、無法辨識的 RunAsGroup=true 給刪除
```
* 完成後，執行下方指令
```
$systemctl daemon-reload
$systemctl start kubelet.service
$systemctl enable kubelet.service
$systemctl status kubelet.service
```
### ansible-playbook: "Failed to update cache: unknown reason"
應該是 apt PUBKEY 過期需要進行更新
* 到出現該狀況的 node 執行下列指令
```
sudo add-apt-repository ppa:webupd8team/y-ppa-manager
sudo apt-get update
sudo apt-get install y-ppa-manager
```

### 撰寫: Gary
1. install cri-dockerd
https://ithelp.ithome.com.tw/m/articles/10291343
https://computingforgeeks.com/install-mirantis-cri-dockerd-as-docker-engine-shim-for-kubernetes/

2. ansible using kK
https://stackoverflow.com/questions/25582740/missing-sudo-password-in-ansible

3. --ignore-preflight-errors=all
https://ithelp.ithome.com.tw/articles/10209787

4. host_key_checking = False
https://blog.yowko.com/ansible-bypass-fingerprint-check/

5. ansible install docker example
https://jiepeng.me/2020/04/22/manual-installation-docker-by-ansible-playbook-on-gce

6. worker node install docker dependencies error
using this commmand to remove apt source file => sudo rm /etc/apt/sources.list.d/docker.list
https://github.com/docker/for-linux/issues/1349

7. manully install k8s guide
https://ithelp.ithome.com.tw/articles/10242123
