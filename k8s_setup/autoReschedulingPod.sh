#!/bin/sh

KUBECTL="/usr/bin/kubectl"

# Get only nodes which are not drained yet
NOT_READY_NODES=$($KUBECTL get nodes | grep -P 'NotReady(?!,SchedulingDisabled)' | awk '{print $1}' | xargs echo)
# Get only nodes which are still drained
READY_NODES=$($KUBECTL get nodes | grep '\sReady,SchedulingDisabled' | awk '{print $1}' | xargs echo)

#####
#METRICES_CHECK_NODE=$($KUBECTL top node | grep '\s<unknown>' | awk '{print $1}' | xargs echo)
NAMESPACES=$($KUBECTL get ns | grep -v "kube\|system\|NAME" |awk '{print $1}' | xargs echo)
#####
echo "Unready nodes that are undrained: $NOT_READY_NODES"
echo "Ready nodes: $READY_NODES"
echo "Metrices check Node Alive : $METRICES_CHECK_NODE"


for node in $NOT_READY_NODES;do
  echo "Node $node not metrices check yet, draining..."
  $KUBECTL drain --ignore-daemonsets --force $node &
  for ns in $NAMESPACES;do
    POD_CHECK_STATUS_TERMINATING=$($KUBECTL get pod -n $ns -o wide | grep $node | awk '{print $1}' | xargs echo)
    for podname in $POD_CHECK_STATUS_TERMINATING;do
      echo "kill pod with terminating status"
      $KUBECTL delete pod $podname -n $ns --force --grace-period 0
      echo "done"
    done;
  done;
  echo "Done"
done;

for node in $NOT_READY_NODES; do
  echo "Node $node not drained yet, draining..."
  $KUBECTL drain --ignore-daemonsets --force $node
  echo "Done"
done;

for node in $READY_NODES; do
  echo "Node $node still drained, uncordoning..."
  $KUBECTL uncordon $node
  echo "Done"
done;

