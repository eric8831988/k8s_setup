#!/bin/sh

echo "rebuild k8s with storageclass default"
echo "start...."

expect expect.sh


echo "deploy StorageClass.."
kubectl apply -f ./local-path-provisioner/deploy/local-path-storage.yaml
kubectl patch storageclass local-path -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
echo "kubernets deploy done.."


### https://blog.csdn.net/Wuli_SmBug/article/details/104712653
### delete worker node cni0 if failed
### sudo ifconfig cni0 down
### sudo ip link delete cni0
